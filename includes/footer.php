<!-- Footer -->
<footer>
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-inline">
                    <li>
                        <a href="#about">link1</a>
                    </li>
                    <li>
                        <a href="#about">link2</a>
                    </li>
                    <li>
                        <a href="#about">link3</a>
                    </li>
                    <li>
                        <a href="#about">link4</a>
                    </li>
                    <li>
                        <a href="#about">link5</a>
                    </li>
                </ul>
                <p class="copyright text-muted small">Copyright &copy; CS76 2016. All Rights Reserved</p>
            </div>
        </div>
    </div>
</footer>