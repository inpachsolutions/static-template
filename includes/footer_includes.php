<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Bootstrap JavaScript -->
<script src="js/bootstrap.min.js" ></script>
<script>
	var url = window.location;
	$('ul.nav a[href="'+ url +'"]').parent().addClass('active');
	$('ul.nav a').filter(function() {
	    return this.href == url;
	}).parent().addClass('active');
</script>
<script src="https://cdn.jsdelivr.net/scrollreveal.js/3.3.1/scrollreveal.min.js"></script>
<script type="text/javascript" src="js/parallax.min.js"></script>
<script src="js/lightbox.min.js"></script>