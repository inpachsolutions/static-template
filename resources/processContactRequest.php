<?php
@session_start();
require_once 'autoload.php';
$siteKey = '6LecXicTAAAAANgvZuOVDUbixsPtVdpzXlb5OnVe';
$secret = '6LecXicTAAAAAMLhwi6VRzlWxoSSCz0USHun9Jzs';

if (isset($_POST['g-recaptcha-response'])){
    $recaptcha = new \ReCaptcha\ReCaptcha($secret);
    $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
    if ($resp->isSuccess()){
        $to = "info@inpachsolutions.com";
        $subject = 'This came from the book a table the UK FM Traders "Get in Touch" form';
        
        $emailbody ='';
        
        $name = '';
        $email = '';
        $mobile = '';
        $comment = '';
        
        if(isset($_POST['name'])){ $name = $_POST['name']; }
        if(isset($_POST['email'])){ $email = $_POST['email']; }
        if(isset($_POST['mobile'])){ $mobile = $_POST['mobile']; }
        if(isset($_POST['comments'])){ $comments = $_POST['comments']; }
        
        $feedback = '';
        
        $message = <<<EMAIL
Hi!
My name is $name
        
From $name
my email id: $email
my contact number: $mobile
Additional comments: $comments
        
EMAIL;
        $header = '$email';
        if ($_POST){
            mail($to,$subject,$message);
            $_SESSION['INFOMSG_ARR'] = 'Thank you for contacting us. We will get back to you soon.';
            header("location: ../contact.php");
            exit();
        }else{
            $_SESSION['ERRMSG_ARR'] = 'Query Failed. Please try after some time';
            header("location: ../contact.php");
            exit();
        }
    }else{
        $_SESSION['ERRMSG_ARR'] = 'Request Failed: Please fill in the CAPTCHA';
        header("location: ../contact.php");
        exit(); 
    }
}
?>