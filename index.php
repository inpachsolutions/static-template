<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Template</title>
		<?php include_once('includes/header_includes.php'); ?>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="main--wrapper">
			<?php include_once('includes/header.php'); ?>
			<div class="page--wrapper">
				<?php include_once('includes/carousel.php'); ?>
				<div class="container">
					<section class="row pad">
						<div class="col-md-4">
			            	<h2>Icon sizes</h2>
				            <hr>
				            <p><i class="fa fa-camera-retro fa-lg"></i> fa-camera-retro</p>

				            <!-- NOTE: 'fa' is Font Awesom, but 'text-danger' is a Bootstrap 3 class -->
				            <p><i class="fa fa-camera-retro fa-2x text-danger"></i> fa-camera-retro</p>
				            <p><i class="fa fa-camera-retro fa-3x text-success"></i> fa-camera-retro</p>
				            <p><i class="fa fa-camera-retro fa-4x text-primary"></i> fa-camera-retro</p>
				            <p><i class="fa fa-camera-retro fa-5x text-muted"></i> fa-camera-retro</p>
			            </div> <!-- /.col-md-4 -->
			            <div class="col-md-4">
			            	<div class="links">
			            		<h2>Important Links</h2>
					            <hr>
					            <p>Content Animation Library: <a target="_blank" href="https://scrollrevealjs.org/"><i class="fa fa-link"></i></a></p>
					            <p>Google ReCaptcha: <a target="_blank" href="https://www.google.com/recaptcha/"><i class="fa fa-link"></i></a></p>
					            <p>Image editor: <a target="_blank" href=http://www.fotor.com/app.html#!module/design/tool/Design"http://www.fotor.com/app.html#!module/design/tool/Design"><i class="fa fa-link"></i></a></p>
					            <p>Favicon: <a target="_blank" href="http://www.favicon-generator.org/"><i class="fa fa-link"></i></a></p>
					            <p>Google analytics <a target="_blank" href="http://analytics.google.com/"><i class="fa fa-link"></i></a></p>
					            <p>Sitemaps <a target="_blank" href="https://www.xml-sitemaps.com/"><i class="fa fa-link"></i></a></p>
					            <p>Robots.txt <a target="_blank" href=""><i class="fa fa-link"></i></a></p>
					            <p>Stock Images <a target="_blank" href="http://unsplash.com"><i class="fa fa-link"></i></a></p>
					            
			            	</div>
			            </div> <!-- /.col-md-4 -->
			            <div class="col-md-4">
			            	<div class="links">
			            		<h2>&nbsp;</h2>
			            		<h4>&nbsp;</h4>
					            <p>UI Gradients: <a target="_blank" href="http://uigradients.com/"><i class="fa fa-link"></i></a></p>
			            	</div>
			            </div> <!-- /.col-md-4 -->
					</section>
				</div>
			</div>
			<div class="parallax-window">
			</div>
			<?php include_once('includes/footer.php'); ?>
		</div>
		<?php include_once('includes/footer_includes.php'); ?>
		<script>
			window.sr = ScrollReveal();
			sr.reveal('.links');
			sr.reveal('.section1', { duration: 1200, reset:false, mobile: true, viewFactor: 0.2 });
			$('.parallax-window').parallax({imageSrc: 'images/p-1400x1400.jpg'});
		</script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	</body>
</html>