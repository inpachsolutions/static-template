<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Template</title>
		<?php include_once('includes/header_includes.php'); ?>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="main--wrapper">
			<?php include_once('includes/header.php'); ?>
			<div class="page--wrapper">
				<div class="container">
					<div class="row pad">
					<!-- Navigation Buttons -->
					<div class="col-md-3">
					<ul class="nav nav-pills nav-stacked" id="myTabs">
					  <li class="active"><a href="#c1" data-toggle="pill">Category1</a></li>
					  <li><a href="#c2" data-toggle="pill">Category2</a></li>
					  <li><a href="#c3" data-toggle="pill">Category3</a></li>
					</ul>
					</div>

					<!-- Content -->
					<div class="col-md-9">
					<div class="tab-content">
					  <div class="tab-pane active" id="c1">
					  	<?php include_once('menu/c1.php'); ?>
					  </div>
					  <div class="tab-pane" id="c2">
					  	<?php include_once('menu/c2.php'); ?>
					  </div>
					  <div class="tab-pane" id="c3">
					  	<?php include_once('menu/c3.php'); ?>
					  </div>
					</div>
					</div>
				</div>
				</div>
			</div>
			<?php include_once('includes/footer.php'); ?>
		</div>
		<?php include_once('includes/footer_includes.php'); ?>
		<script>
			window.sr = ScrollReveal();
			sr.reveal('.links');
			sr.reveal('.section1', { duration: 1200, reset:false, mobile: true, viewFactor: 0.2 });
			$('.parallax-window').parallax({imageSrc: 'img/p-1400x1400.jpg'});
		</script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	</body>
</html>