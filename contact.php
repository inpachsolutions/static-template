<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Template</title>
		<?php include_once('includes/header_includes.php'); ?>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="main--wrapper">
			<?php include_once('includes/header.php'); ?>
			<div class="page--wrapper">
				<div class="container-fluid">
					<div class="row pad">
						<div class="col-md-8">
							<iframe
							  width="100%"
							  height="500"
							  frameborder="0" style="border:0"
							  src="https://www.google.com/maps/embed/v1/place?key= AIzaSyD_JN40ZMFSHXwqHBdEB6t3ySkx8NHW30Y&q=Space+Needle,Seattle+WA" allowfullscreen>
							</iframe>
							<p><a target="_blank" href="https://developers.google.com/maps/documentation/embed/guide">Google Maps API</a></p>
						</div>

						<div class="col-md-4">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Contact us</h3>
								</div>
								<div class="panel-body">
									<div>
									<?php
							            @session_start();
							            if (isset ( $_SESSION ['ERRMSG_ARR'] )) {
							              echo '<div class="alert alert-danger" role="alert">
							              <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							              <span class="sr-only">Error:</span>' . $_SESSION ['ERRMSG_ARR'] . '</div>';
							              unset ( $_SESSION ['ERRMSG_ARR'] );
							            }
							            
							            if (isset ( $_SESSION ['INFOMSG_ARR'] )) {
							              echo '<div class="alert alert-success" role="alert">' . $_SESSION ['INFOMSG_ARR'] . '</div>';
							              unset ( $_SESSION ['INFOMSG_ARR'] );
							            }
							        ?>
							        </div>
							        <form role="form" action="resources/processContactRequest.php" method="post" >
										<div class="form-group">
											<label for="InputName">Your Name</label>
											<input type="text" class="form-control" name="name" id="InputName" placeholder="Enter Name" required>
										</div>
										<div class="form-group">
											<label for="InputEmail">Your Email</label>
											<input type="email" class="form-control" id="InputEmail" name="email" placeholder="Enter Email" required  >
										</div>
										<div class="form-group">
											<label for="InputEmail">Your Mobile</label>
											<input type="integer" class="form-control" id="mobile" name="mobile" placeholder="Enter Mobile Number" required  >
										</div>
										<div class="form-group">
											<label for="InputMessage">Message</label>
											<textarea name="comment"class="form-control" rows="2" required></textarea>
										</div>
								        <div class="form-group">
	                                    	<div class="col-md-12">
	                                    		<div class="row">
		                                    		<div class="g-recaptcha" data-sitekey="6LecXicTAAAAANgvZuOVDUbixsPtVdpzXlb5OnVe"></div>
		                                    	</div>
	                                    	</div>
	                                    </div>
	                                    <div class="col-md-12">
	                                    	&nbsp;
	                                    </div>
										<div class="form-group">
											<input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info form-control">
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php include_once('includes/footer.php'); ?>
		</div>
		<?php include_once('includes/footer_includes.php'); ?>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	</body>
</html>