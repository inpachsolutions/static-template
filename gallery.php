<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Template</title>
		<?php include_once('includes/header_includes.php'); ?>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="main--wrapper">
			<?php include_once('includes/header.php'); ?>
			<div class="page--wrapper">
				<div class="container">
					<div class="row pad">
						<div class="col-md-4 no-pad">
							<a href="images/slide1.jpg" data-lightbox="roadtrip">
								<img src="images/slide1.jpg" class="img-responsive">
							</a>
						</div>
						<div class="col-md-4 no-pad">
							<a href="images/slide2.jpg" data-lightbox="roadtrip">
								<img src="images/slide2.jpg" class="img-responsive">
							</a>
						</div>
						<div class="col-md-4 no-pad">
							<a href="images/slide3.jpg" data-lightbox="roadtrip">
								<img src="images/slide3.jpg" class="img-responsive">
							</a>
						</div>
						<div class="col-md-4 no-pad">
							<a href="images/p-1400x1400.jpg" data-lightbox="roadtrip">
								<img src="images/p-1400x1400.jpg" class="img-responsive">
							</a>
						</div>

					</div>
				</div>
			</div>
			<?php include_once('includes/footer.php'); ?>
		</div>
		<?php include_once('includes/footer_includes.php'); ?>
		<script>
			window.sr = ScrollReveal();
			sr.reveal('.links');
			sr.reveal('.section1', { duration: 1200, reset:false, mobile: true, viewFactor: 0.2 });
			$('.parallax-window').parallax({imageSrc: 'img/p-1400x1400.jpg'});
		</script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	</body>
</html>